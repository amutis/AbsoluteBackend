<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountLine extends Model
{
    use SoftDeletes;

    public function company()
    {
        return $this->hasOne('App\Company','id','company_id');
    }
}
