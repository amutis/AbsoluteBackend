<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignmentCost extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function vehicle_assignment()
    {
        return $this->hasOne('App\VehicleAssignment','id','vehicle_assignment_id');
    }
}
