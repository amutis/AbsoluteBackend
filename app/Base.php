<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Base extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function traffic()
    {
        return $this->hasMany('App\BaseTraffic','base_id','id');
    }
}
