<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BaseTraffic extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function base()
    {
        return $this->hasOne('App\Base','id','base_id');
    }

    /**
     *
     **/
    public function vehicle_assignment()
    {
        return $this->hasOne('App\VehicleAssignment','vehicle_assignment_id','id');
    }
}
