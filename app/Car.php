<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Car extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function vehicle_type()
    {
        return $this->hasOne('App\VehicleType','id','vehicle_type_id');
    }

    /**
     *
     **/
    public function owner()
    {
        return $this->hasOne('App\Owner','id','owner_id');
    }

    /**
     *
     **/
    public function vehicle_assignments()
    {
        return $this->hasOne('App\VehicleAssingment','vehicle_id','id');
    }

    /**
     *
     **/
    public function today_assignments()
    {
        return $this->hasOne('App\VehicleAssingment','vehicle_id','id')->whereDate('created_at',date('Y-m-d'));
    }
}
