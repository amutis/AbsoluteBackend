<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function country()
    {
        return $this->hasOne('App\Country','id','country_id');
    }

    /**
     *
     **/
    public function rate_detail()
    {
        return $this->hasOne('App\Rate','company_id','id');
    }

    /**
     *
     **/
    public function users()
    {
        return $this->hasMany('App\User','company_id','id');
    }

    /**
     *
     **/
    public function account_lines()
    {
        return $this->hasMany('App\AccountLine','company_id','id');
    }

    /**
     *
     **/
    public function no_go_zones()
    {
        return $this->hasMany('App\NoGoZone','company_id','id');
    }

}
