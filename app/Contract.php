<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contract extends Model
{
    use SoftDeletes;

    public function tariff()
    {
        return $this->hasOne('App\Tarrif','contract_id','id');
    }
}
