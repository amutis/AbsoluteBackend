<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cost extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function ride_costs()
    {
        return $this->hasMany('App\RideCost','cost_type_id','id');
    }

    /**
     *
     **/
    public function assignment_costs()
    {
        return $this->hasMany('App\AssignmentCost','cost_type_id','id');
    }

}
