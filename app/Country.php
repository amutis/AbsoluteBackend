<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     *
     **/
    public function companies()
    {
        return $this->hasMany('App\Company','country_id','id');
    }
}
