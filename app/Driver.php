<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Driver extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function user_detail()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    /**
     *
     **/
    public function vehicle_assignments()
    {
        return $this->hasMany('App\VehicleAssingment','driver_id','id');
    }

    /**
     *
     **/
    public function today_vehicle_assignments()
    {
        return $this->hasMany('App\VehicleAssingment','driver_id','id')->whereDate('created_at',date('Y-m-d'));
    }


    //API

    /**
     *
     **/
    public function api_user_detail()
    {
        return $this->hasOne('App\User','id','user_id')->select('id','first_name','last_name','email','phone_number');
    }
}
