<?php

namespace App\Http\Controllers\Api;

use App\Ride;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Commissions extends Controller
{
    /**
     *
     **/
    public function getCommission()
    {
        $user = Auth::user();
//        $user = User::find(2);

        if ($user->user_type_id == 4){
            $driver = $user->driver;

            $rides = Ride::where('driver_id',$driver->id)->where('status',2)->whereDate('date_time',date('Y-m-d'))->get();

            $response = array(
                'upcoming_trips' => $rides,
                'total_commission' => 1000
            );

            return response($response,200);

        }else{
            $error = "Your account is not a driver account";
            echo json_encode($error);
        }
    }
}
