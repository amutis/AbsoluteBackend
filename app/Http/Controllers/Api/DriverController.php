<?php

namespace App\Http\Controllers\Api;

use App\Ride;
use App\User;
use App\VehicleAssingment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DriverController extends Controller
{

    public function __construct()
    {
//        $this->middleware('driver');
    }

    /**
     *
     **/
    public function assignment_details()
    {
        $user = Auth::user();

        if ($user->user_type_id == 4){
            $driver = $user->driver;
            $assignment = VehicleAssingment::where('driver_id',$driver->id)->whereDate('created_at',date('Y-m-d'))->first();
            if ($assignment != null){
                    $details = [];
                    array_push($details,[
                       'user_details' =>  $user,
                       'license_number' =>  $user->api_driver->license_number,
                       'vehicle' =>  $assignment->api_car->number_plate,
                       'done_trips' =>  Ride::whereDate('date_time',date('Y-m-d'))->where('vehicle_assignment_id',$assignment->id)->where('status',2)->get(),
                       'upcoming_trips_today' =>  Ride::where('vehicle_assignment_id',$assignment->id)->where('status',0)->get(),
                       'future_trips' =>  Ride::whereDate('date_time','>',date('Y-m-d'))->where('driver_id',$driver->id)->where('status',0)->get()
                    ]);
                    return $details;
            }else{
                $error = "You have not been assigned a vehicle yet. Contact control room";
                echo json_encode($error);
            }
        }else{
            $error = "Your account is not a driver account";
            echo json_encode($error);
        }
    }

    /**
     *
     **/
    public function getCommissions()
    {
//        $user = Auth::user();
        $user = User::find(2);

        if ($user->user_type_id == 4){
            $driver = $user->driver;

            $rides = Ride::where('driver_id',$driver->id)->where('status',2)->whereDate('date_time',date('Y-m-d'))->get();

            $data = array(
              [
                  "trips_today" => $rides
              ]
            );

            return $data;
        }else{
            $error = "Your account is not a driver account";
            echo json_encode($error);
        }
    }


}
