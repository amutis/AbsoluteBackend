<?php

namespace App\Http\Controllers\Api;

use App\Jobs\MobileNotification;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class MobileNotifications extends Controller
{
    /**
     *
     **/
    public function update_fcm(Request $request)
    {
        $this->validate($request, array(
            'fcm' => 'required',
        ));

        $user = User::find(Auth::user()->id);
        $user->fcm = $request->fcm;
        $user->save();

        $message = [];
        array_push($message,[
            'status' => 'fcm saved'
        ]);

        return response($message,200);
    }

    /**
     *
     **/
    public function send_user_notification()
    {
        $user_id = 1;
        $message = ['title'=>'title of notification',
                    'message'=>'this is the message'
                    ];
        $notification = (new MobileNotification($user_id,$message));
        dispatch($notification);

        return "done";

    }

    /**
     *
     **/
    public function check_notification()
    {

        $user_id = Auth::user()->id;
        $message = ['title'=>'title of notification',
            'message'=>'this is the message'
        ];
        $notification = (new MobileNotification($user_id,$message));
        dispatch($notification);

        return "Notification sent";
    }
}
