<?php

namespace App\Http\Controllers\Api;

use App\Driver;
use App\Ride;
use App\VehicleAssingment;
use App\VehicleStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PositionsController extends Controller
{
    /**
     *
     **/
    public function update_position(Request $request)
    {
        $this->validate($request, array(
            'lat' => 'required',
            'long' => 'required',
        ));

        $vehicle_assignment_id = VehicleAssingment::where('driver_id',Auth::user()->driver->id)->whereDate('created_at',date('Y-m-d'))->first()->id;

       if ($vehicle_assignment_id != null){
           $last_position = VehicleStatus::where('vehicle_assignment_id',$vehicle_assignment_id)->where('status',0)->first();

           //condition if available or not
           if ($last_position != null){
               //there was a last position available

               //change last position status
               $last_position->status = 1;
               $last_position->save();

               //create a new position
               $new_position = new VehicleStatus();
               $new_position->vehicle_assignment_id = $vehicle_assignment_id;
               $new_position->lat = $request->lat;
               $new_position->long = $request->long;
               $new_position->save();

               return response('position updated',200);
           }else{
               //create a new position
               $new_position = new VehicleStatus();
               $new_position->vehicle_assignment_id = $vehicle_assignment_id;
               $new_position->lat = $request->lat;
               $new_position->long = $request->long;
               $new_position->save();

               return response('first position of the day added',200);
           }
       }else{
           $message = array(
               'message' => 'Vehicle not assigned yet'
           );
          return response($message,200);
       }

    }

    /**
     *
     **/
    public function position_of_coming_car($trip_id)
    {

        //get Trip
        $trip = Ride::find($trip_id);
        //checking if trip is available
        if ($trip == true){
            if ($trip->status == 1){
                $message = array(
                    'message' => 'trip already in session. Use /api/ride/find_trip_location/{trip_id}'
                );

                return response($message,200);
            }
            if ($trip->status == 2){
                $message = array(
                    'message' => 'trip already ended.'
                );

                return response($message,200);
            }
        }else{
            $message = array(
                'message' => 'are you trying to be superman? that trip doesn\'t exist'
            );

            return response($message,200);
        }

        //checking if requester owns the ride
        if ($trip->user_id != Auth::user()->id){
            $message = array(
                'message' => 'We are sorry but that ride is not yours.'
            );

            return response($message,200);
        }


        $vehicle_assignment_id = $trip->vehicle_assignment_id;
        //check for trip to get vehicle assignment
        if ($vehicle_assignment_id != null){
            //get vehicle status
            $vehicle_status = VehicleStatus::where('vehicle_assignment_id',$vehicle_assignment_id)->where('status',0)->first();

            if ($vehicle_status == null){
                $message = array(
                    'message' => 'Cannot locate driver'
                );
                return response($message,200);
            }else{
                $message = array(
                    'last_vehicle_position' => $vehicle_status,
                    'driver_details' => Driver::where('id',$trip->driver_id)->with('user_detail')->first(),
                    'vehicle_details' => VehicleAssingment::find($vehicle_assignment_id)->car
                );

                return response($message,200);
            }

        }else{
            $message = array(
                'message' => 'Trip does not have any assignment'
            );
            return response($message,200);
        }
    }

    /**
     *
     **/
    public function position_of_trip($trip_id)
    {

        //get Trip
        $trip = Ride::find($trip_id);
        //checking if trip is available
        if ($trip == true){
            if ($trip->status != 1){
                $message = array(
                    'message' => 'that trip has not been started. Use /api/ride/find_driver_location/{trip_id}'
                );

                return response($message,200);
            }
        }else{
            $message = array(
                'message' => 'are you trying to be superman? that trip doesn\'t exist'
            );

            return response($message,200);
        }

        //checking if requester owns the ride
        if ($trip->user_id != Auth::user()->id){
            $message = array(
                'message' => 'We are sorry but that ride is not yours.'
            );

            return response($message,200);
        }

        $vehicle_assignment_id = $trip->vehicle_assignment_id;
        //check for trip to get vehicle assignment
        if ($vehicle_assignment_id != null){
            //get vehicle status
            $vehicle_status = VehicleStatus::where('vehicle_assignment_id',$vehicle_assignment_id)->where('status',0)->first();
            if ($vehicle_status == null){
                $message = array(
                    'message' => 'We are finding it hard to locate you.'
                );
                return response($message,200);
            }else{
                $message = array(
                    'last_vehicle_position' => $vehicle_status,
                    'driver_details' => Driver::where('id',$trip->driver_id)->with('user_detail')->first(),
                    'vehicle_details' => VehicleAssingment::find($vehicle_assignment_id)->car
                );

                return response($message,200);
            }
        }else{
            $message = array(
                'message' => 'Trip does not have any assignment'
            );
            return response($message,200);
        }
    }
}
