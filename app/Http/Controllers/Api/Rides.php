<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Rides extends Controller
{
    /**
     *
     **/
    public function request_ride(Request $request)
    {
        $this->validate($request, array(
            'ride_date' => 'required',
            'ride_time' => 'required',
//            'passenger' => 'required',
        ));
//
        $departure = $request->departure;
        $destination = $request->destination;
//        return "nothing";
        $time =  date('H:i',strtotime($request->ride_time)).":00";


        $address = str_replace(" ", "+", $departure); // replace all the white space with "+" sign to match with google search pattern
        $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
        $response = file_get_contents($url);
        $json = json_decode($response,TRUE); //generate array object from the response from the web
        $departure_co = ($json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng']);

        $address = str_replace(" ", "+", $destination); // replace all the white space with "+" sign to match with google search pattern
        $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
        $response = file_get_contents($url);
        $json = json_decode($response,TRUE); //generate array object from the response from the web
        $destination_co = ($json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng']);


        $ride = new Ride();
//        TODO change to normal session
        $ride->session_id = Auth::user()->id;
        $ride->ride_type_id = 2;
        $ride->departure_co = $departure_co;
        $ride->departure_name = $departure;
        $ride->destination_co = $destination_co;
        $ride->destination_name = $destination;
        $ride->date_time = $request->ride_date." ".$time;
//        TODO change name
        $ride->client_name = "Trial Passenger";
        $ride->user_id = Auth::user()->id;
//        $ride->user_id = decrypt($request->passenger);
        $ride->save();

        return redirect()->route('admin.trips.un_assigned');
    }
}
