<?php

namespace App\Http\Controllers\Api;

use App\Jobs\Sos;
use App\Ride;
use App\SosContact;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SosController extends Controller
{
    public function doSos(Request $request)
    {
        $this->validate($request, array(
            'lat' => 'required',
            'lng' => 'required',
        ));


        $user = Auth::user();
//        $car = Ride::find($request->ride_id)->assignment->car->number_plate;
        $people = SosContact::all();
        foreach ($people as $person)  {
        $mobile = $person->phone_number;
        $message = 'An SOS request has been made by '.$user->first_name.' '.$user->last_name.'. Location : http://maps.google.com/maps?q='.$request->lat.','.$request->lng.'&ll='.$request->lat.','.$request->lng.'&z=17';
        dispatch(new Sos($mobile,$message));
        }


        return "message sent";
    }
}
