<?php

namespace App\Http\Controllers\Api;

use App\Ride;
use App\StoredPlace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StoredPlacesController extends Controller
{
    public function store_place(Request $request)
    {
        $this->validate($request, array(
            'type' => 'required',
            'ride_id' => 'required'
        ));
        // 1 is departure place 2 is destination
        if ($request->type == 1){

            $store = new StoredPlace();
            $store->user_id = Auth::user()->id;
            $store->ride_id = $request->ride_id;
            $store->location = Ride::find($request->ride_id)->departure_name;
            $store->latlng = Ride::find($request->ride_id)->departure_co;
            $store->save();

            $response = array(
                'message' => 'Departure stored',
            );
            return response($response,200);

        }elseif ($request->type == 2){

            $store = new StoredPlace();
            $store->user_id = Auth::user()->id;
            $store->ride_id = $request->ride_id;
            $store->location = Ride::find($request->ride_id)->destination_name;
            $store->latlng = Ride::find($request->ride_id)->destination_co;
            $store->save();

            $response = array(
                'message' => 'Destination stored',
            );
            return response($response,200);
        }
    }

    public function stored_places()
    {
        $user = Auth::user()->id;

        $stored_place = StoredPlace::where('user_id',$user)->get();

        $response = array(
            'stored_places' => $stored_place,
        );
        return response($response,200);
    }
}
