<?php

namespace App\Http\Controllers\Api;

use App\Driver;
use App\Jobs\MobileNotification;
use App\Mail\PrivateInvoice;
use App\Ride;
use App\SystemDefault;
use App\Tarrif;
use App\VehicleAssingment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;

class Trip extends Controller
{
    /**
     *
     **/
    public function request_ride(Request $request)
    {
        $this->validate($request, array(
            'ride_date' => 'required',
            'ride_time' => 'required',
            'departure_place_name' => 'required',
            'destination_place_name' => 'required',
        ));

        $departure = $request->departure_place_name;
        $destination = $request->destination_place_name;

        $time =  date('H:i',strtotime($request->ride_time)).":00";


        $address = str_replace(" ", "+", $departure); // replace all the white space with "+" sign to match with google search pattern

        $departure_co = Cache::remember($address, 300, function () use ($address) {
            $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
            $response = file_get_contents($url);
            $json = json_decode($response,TRUE); //generate array object from the response from the web
            $departure_co = ($json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng']);

            return $departure_co;
        });


        $address = str_replace(" ", "+", $destination); // replace all the white space with "+" sign to match with google search pattern

        $destination_co = Cache::remember($address, 300, function () use ($address) {
            $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
            $response = file_get_contents($url);
            $json = json_decode($response,TRUE); //generate array object from the response from the web
            $destination_co = ($json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng']);

            return $destination_co;
        });

        $departure = str_replace(array("\r", "\r\n", "\n"),', ',trim($departure));
        $destination = str_replace(array("\r", "\r\n", "\n"),', ',trim($destination));

        $ride = new Ride();
        $ride->session_id = 1;
        $ride->ride_type_id = 2;
        $ride->departure_co = $departure_co;
//        $ride->vehicle_type_id = $request->vehicle_type_id;
        $ride->departure_name = $departure;
        $ride->destination_co = $destination_co;
        $ride->destination_name = $destination;
        $ride->date_time = $request->ride_date." ".$time;
        $ride->client_name = Auth::user()->first_name.' '.Auth::user()->last_name;
        $ride->user_id = Auth::user()->id;
        if ($request->note == true){
            $ride->note = $request->note;
        }
        $ride->save();

        $messages = ['title'=>'Ride Booked',
            'message'=>'Ride booked. We will notify you on ride assignment'
        ];
        if (\App\User::find(Auth::user()->id)->fcm !=null){
            $user_id = Auth::user()->id;


            $notification = (new MobileNotification($user_id,$messages));

            dispatch($notification);
        }




        $message = [];
        array_push($message,[
            'status' => 'ride booked',
            'ride_details' => $ride,
            'message' => $messages
        ]);

        return response($message,200);
    }


    /**
     *
     **/
    public function getRideDistance($id)
    {
        $ride = Ride::find($id);
        $departure_name = $ride->departure_name;
        $departure_name = str_replace(" ", "+", $departure_name);
        $destination_name = $ride->destination_name;
        $destination_name = str_replace(" ", "+", $destination_name);
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$departure_name."&destinations=".$destination_name."&sensor=false";
        $response = file_get_contents($url);
        $json = json_decode($response,TRUE); //generate array object from the response from the web
        $distance = str_replace(" mi", "",($json['rows'][0]['elements'][0]['distance']['text']));
        $eta = ($json['rows'][0]['elements'][0]['duration']['text']);
        $result = array(
            "distance" => $distance.' km',
            "eta" => $eta
        );
        return response($result,200);
    }

    /**
     *
     **/
    public function getRides()
    {
        $rides = Ride::where('user_id',Auth::user()->id)->get();
        $trips = [];
        foreach ($rides as $ride){
            if ($ride->driver_id == null){
                $driver = 'no driver yet';
            }else{
                $driver = $ride->driver->api_user_detail;
            }
            if ($ride->vehicle_assignment_id == null){
                $vehicle = 'no vehicle assigned';
            }else{
                $vehicle = $ride->assignment->car->number_plate;
            }
            if($ride->company_id == null){
                $company = 'no company';
            }else{
                $company = $ride->company->name;
            }
            array_push($trips,
                [
                    'id' => $ride->id,
//                    'person_who_booked' => $ride->api_session,
                    'passenger' => $ride->api_passenger,
                    'driver' => $driver,
                    'vehicle' => $vehicle,
                    'ride_type' => $ride->api_type,
                    'company' => $company,
                    'departure_name' => $ride->departure_name,
                    'departure_co' => $ride->departure_co,
                    'destination_name' => $ride->destination_name,
                    'destination_co' => $ride->destination_co,
                    'amount' => $ride->amount,
                    'ride_date_time' => $ride->date_time,
                    'departure_time' => $ride->departure_time,
                    'arrival_time_time' => $ride->arrival_time,
                    'note' => $ride->note
                ]
                );
        }
        return response($trips,200);
    }


    /**
     *
     **/
    public function upcoming_trips()
    {
        $user = Auth::user();

        if ($user->user_type_id == 4){
            $assignment_id = VehicleAssingment::where('driver_id',$user->driver->id)->whereDate('created_at',date('Y-m-d'))->first()->id;

            if ($assignment_id != null){
                $rides = Ride::whereDate('date_time',date('Y-m-d'))->where('vehicle_assignment_id',$assignment_id)->where('status',0)->get();
                $trips = [];
                foreach ($rides as $ride){
                    array_push($trips,
                        [
                            'id' => $ride->id,
                            'person_who_booked' => $ride->api_session,
                            'passenger' => $ride->api_passenger,
                            'ride_type' => $ride->api_type,
//                            'company' => $ride->company->name,
                            'departure_name' => $ride->departure_name,
                            'departure_co' => $ride->departure_co,
                            'destination_name' => $ride->destination_name,
                            'destination_co' => $ride->destination_co,
                            'ride_date_time' => $ride->date_time,
                            'note' => $ride->note,
                        ]
                    );
                }
                return response($trips,200);
            }else{
                $error = "You have not been assigned a vehicle yet. Contact control room";
                echo json_encode($error);
            }

        }else{
            $rides = Ride::where('status','<',2)->with('ride_type')->get();


            $response = array(
                'upcoming_trips' => $rides,
            );
            return response($response,200);
        }
    }

    /**
     *
     **/
    public function future_trips()
    {
        $user = Auth::user();

        if ($user->user_type_id == 4){

                $rides = Ride::where('status',0)->whereDate('date_time','>',date('Y-m-d'))->where('driver_id',$user->driver->id)->get();
                $trips = [];
                foreach ($rides as $ride){
                    array_push($trips,
                        [
                            'id' => $ride->id,
                            'person_who_booked' => $ride->api_session,
                            'passenger' => $ride->api_passenger,
                            'ride_type' => $ride->api_type,
//                            'company' => $ride->company->name,
                            'departure_name' => $ride->departure_name,
                            'departure_co' => $ride->departure_co,
                            'destination_name' => $ride->destination_name,
                            'destination_co' => $ride->destination_co,
                            'ride_date_time' => $ride->date_time,
                            'note' => $ride->note,
                        ]
                    );
                }
                return response($trips,200);
        }else{
            $error = "Your account is not a driver account";
            echo json_encode($error,200);
        }
    }

    /**
     *
     **/
    public function trip_start(Request $request)
    {
        //validate
        $this->validate($request, array(
            'id' => 'required',
            'coordinates' => 'required'
        ));

        //get trip
        $trip = Ride::find($request->id);
        $driver = $trip->driver_id;
        if ($driver != null){
            $driver_user = $trip->driver->user_id;

            //check if trip is assigned to the logged in driver
            if (Auth::user()->id == $driver_user){

                //get vehicle assignment
                $vehicle_assignment = VehicleAssingment::where('driver_id',$driver)->whereDate('created_at',date('Y-m-d'))->first();

                //mark driver as un available
                $vehicle_assignment->status = 1;
                $vehicle_assignment->save();

                //update departure place
                $trip->departure_co = $request->coordinates;
                $trip->status = 1;
                $trip->departure_time = date('H:i:s');
                $trip->save();

                $message = array(
                    'message' => 'Trip started'
                );
                return response($message,200);
            }else{
                $message = array(
                    'message' => 'The trip is not assigned to the current driver'
                );
                return response($message,200);
            }

        }else{
            $message = array(
                'message' => 'No driver has been assigned this trip'
            );
            return response($message,200);
        }
    }

    /**
     *
     **/
    public function end_trip(Request $request)
    {
        //validate
        $this->validate($request, array(
            'id' => 'required',
            'coordinates' => 'required',
            'km' => 'required',
            'time' => 'required'
        ));

        //get trip
        $trip = Ride::find($request->id);
        $message = [];

        //check if trip is in session
        if ($trip->status == 1){
            $vehicle_assignment = VehicleAssingment::where('driver_id',$trip->driver_id)->whereDate('created_at',date('Y-m_d'))->first();

                $cost = 0;

            if ($trip->ride_type_id == 1){
                //Corporate ride

                //mark driver as available
                $vehicle_assignment->status = 0;
                $vehicle_assignment->save();

                //update destination place in case it changed
                $trip->destination_co = $request->coordinates;
                $trip->status = 2;

                //Getting the tarrif
                $tarrif = Tarrif::where('company_id',$trip->company->id )->where('status',0)->first();
                $km_tarrif = $tarrif->km_price;
                $min_tarrif = $tarrif->time_price;
                $minimum = $tarrif->min;

                $time_cost = $min_tarrif*$request->time;
                $km_cost = $km_tarrif*$request->km;
                $price = $time_cost+$km_cost;
                //rounding off

                if($minimum > $price){
                    $cost = $minimum;
                }else{
                    $cost = $price;
                }

                $trip->amount = $cost;
                $trip->duration = $request->time;
                $trip->distance = $request->km;
                $trip->tariff_id = $tarrif->id;
                $trip->arrival_time = date('H:i:s');
                $trip->status = 2;
                if ($request->note == true){
                    $trip->driver_note = $request->note;
                }
                $trip->save();

                Mail::to($trip->company->email) ->queue(new CompanyInvoice($trip));

            }elseif($trip->ride_type_id == 2){
                //Private Ride

                //mark driver as available
                $vehicle_assignment->status = 0;
                $vehicle_assignment->save();

                //update destination place in case it changed
                $trip->destination_co = $request->coordinates;
                $trip->status = 2;

                //Getting the tarrif
                $km_tarrif = SystemDefault::find(1)->amount;
                $min_tarrif = SystemDefault::find(2)->amount;
                $minimum = SystemDefault::find(3)->amount;

                $time_cost = $min_tarrif*$request->time;
                $km_cost = $km_tarrif*$request->km;
                $price = $time_cost+$km_cost;
                //rounding off

                if($minimum > $price){
                    $cost = $minimum;
                }else{
                    $cost = $price;
                }

                $trip->amount = $cost;
                $trip->duration = $request->time;
                $trip->distance = $request->km;
                $trip->arrival_time = date('H:i:s');
                $trip->status = 2;
                $trip->save();

                Mail::to($trip->passenger->email) ->queue(new PrivateInvoice($trip));
            }



            $message = array(
                'message' => 'Trip completed',
                'cost' => 'Ksh '.$cost
            );
            return response($message,200);
        }else{
            $message = array(
                'message' => 'You are trying to end a trip that is not in session'
            );
           return response($message,200);
        }

    }


}
