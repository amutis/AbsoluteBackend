<?php

namespace App\Http\Controllers\Api;

use App\Mail\WelcomeEmail;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    /**
     *
     **/
    public function register_user(Request $request)
    {
        $this->validate($request, array(
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'email' => 'required|email|max:255|unique:users',
            'phone_number' => 'required|max:12|min:12|unique:users',
            'password' => 'required'
        ));

        $user = new User();
        $user->user_type_id = 1;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->password = bcrypt($request->password);
        $user->save();

        Mail::to($user->email) ->queue(new WelcomeEmail($user));

        $response = array(
            'message' => 'user_created',
            'user_details' => $user
        );
        return response($response,200);
    }

    /**
     *
     **/
    public function reset_password(Request $request)
    {
        $this->validate($request, array(
            'old_password' => 'required',
            'new_password' => 'required'
        ));


        if (Hash::check(Input::get('old_password'), Auth::user()->password)) {

            $user = User::find(Auth::user()->id);
            $user->password = bcrypt($request->new_password);
            $user->save();

            $response = array(
                'message' => 'Password Changed',
                'password change' => true
            );
            return response($response,200);
        }else{
            $response = array(
                'message' => 'Wrong user or password',
                'password change' => false
            );
            return response($response,200);
        }
    }
}
