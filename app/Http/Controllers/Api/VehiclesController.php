<?php

namespace App\Http\Controllers\Api;

use App\VehicleAssingment;
use App\VehicleType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VehiclesController extends Controller
{
    /**
     *
     **/
    public function available_vehicles()
    {
        $assignments = VehicleAssingment::where('status',0)->get();
        $available = [];
        foreach ($assignments as $assignment){
            array_push($available,[
               'driver' => $assignment->driver->api_user_detail,
               'car' => $assignment->api_car,
               'vehicle_type' => $assignment->car->vehicle_type->name,
               'position' => $assignment->api_position,
            ]);
        }
        return $available;
    }

    /**
     *
     **/
    public function get_vehicle_types()
    {
        $vehicle_types = VehicleType::all();

        $message = array(
            'vehicle_types' => $vehicle_types
        );
        return response($message,200);
    }
}
