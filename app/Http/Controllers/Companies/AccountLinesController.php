<?php

namespace App\Http\Controllers\Companies;

use App\AccountLine;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class AccountLinesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function company($id)
    {

        $data = AccountLine::where('company_id',Company::where('public_id',$id)->first()->id)->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'account_line_name' => 'required',
            'description' => 'required',
            'company_id' => 'required',
        ));

        $account_line = new AccountLine();
        $account_line->public_id = Str::random(8);
        $account_line->company_id = Company::where('public_id',$request->company_id)->first()->id;
        $account_line->name = $request->account_line_name;
        $account_line->description = $request->description;
        $account_line->save();

        $response = array(
            'message' => 'Account Line Added'
        );

        return response($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ride = AccountLine::where('public_id',$id)->first()->id;

        AccountLine::destroy($ride);

        $response = array(
            'message' => 'Account Line deleted'
        );

        return response($response,200);
    }

    //
}
