<?php

namespace App\Http\Controllers\Companies;

use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;
use Illuminate\Support\Str;

class CompaniesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Company::with('country')->orderBy('name', 'asc')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'institution_name' => 'required',
            'country' => 'required',
            'town' => 'required',
            'street' => 'required',
            'building' => 'required',
            'email' => 'required|email|unique:companies',
            'telephone' => 'required|unique:companies,telephone_1|max:12|min:12',
        ));

        $company = new Company();
        $company->public_id = Str::random(8);
        $company->name = $request->institution_name;
        $company->email = $request->email;
        $company->slug =  strtolower(str_replace(' ', '_', $request->institution_name).'_'.rand(1,10));
        $company->telephone_1 = $request->telephone;
        $company->country_id = Country::where('public_id',$request->country)->first()->id;
        $company->town = $request->town;
        $company->street = $request->street;
        $company->building = $request->building;
        $company->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Company::withTrashed()->where('public_id',$id)->with('country','account_lines','no_go_zones')->first();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'institution_name' => 'required',
            'country' => 'required',
            'town' => 'required',
            'street' => 'required',
            'building' => 'required',
            'email' => 'required|email',
            'telephone' => 'required|max:12|min:12',
        ));

        $company = Company::where('public_id',$id)->first();
        $company->name = $request->institution_name;
        $company->email = $request->email;
        $company->telephone_1 = $request->telephone;
        $company->country_id = Country::where('public_id',$request->country)->first()->id;
        $company->town = $request->town;
        $company->street = $request->street;
        $company->building = $request->building;
        $company->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::where('public_id',$id)->first()->id;

        Company::destroy($company);

        $data = Company::with('country')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    //
}
