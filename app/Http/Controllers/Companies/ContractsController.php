<?php

namespace App\Http\Controllers\Companies;

use App\Company;
use App\Contract;
use App\Tarrif;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class ContractsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function company($id)
    {
        $data = Contract::where('company_id',Company::where('public_id',$id)->first()->id)->with('tariff')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, array(
            'contract_name' => 'required',
            'minimum_price' => 'required',
            'price_per_km' => 'required',
            'price_per_min' => 'required',
            'expiry_date' => 'required|date',
        ));

        $id = Company::where('public_id',$id)->first()->id;

//        TODO make job to expire the contract
        $contract = new Contract();
        $contract->public_id = Str::random(8);
        $contract->name = $request->contract_name;
        $contract->contract_expiry = $request->expiry_date;
        $contract->company_id = $id;
        $contract->status = 1;
        $contract->save();

        $tarrif = new Tarrif();
        $tarrif->public_id = Str::random(8);
        $tarrif->company_id = $id;
        $tarrif->contract_id = $contract->id;
        $tarrif->min = $request->minimum_price;
        $tarrif->km_price = $request->price_per_km;
        $tarrif->time_price = $request->price_per_min;
        $tarrif->save();


        $response = array(
            'message' => 'Contract Added'
        );

        return response($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Contract::where('public_id',$id)->with('tariff')->first();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'contract_name' => 'required',
            'minimum_price' => 'required',
            'price_per_km' => 'required',
            'price_per_min' => 'required',
            'expiry_date' => 'required|date',
        ));

//        $id = Company::where('public_id',$id)->first()->id;

//        TODO make job to expire the contract
        $contract = Contract::where('public_id',$id)->first();
        $contract->name = $request->contract_name;
        $contract->contract_expiry = $request->expiry_date;
        $contract->save();

        $tarrif = Tarrif::where('contract_id', $contract->id)->first();
        $tarrif->min = $request->minimum_price;
        $tarrif->km_price = $request->price_per_km;
        $tarrif->time_price = $request->price_per_min;
        $tarrif->save();


        $response = array(
            'message' => 'Contract Updated'
        );

        return response($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ride = Contract::where('public_id',$id)->first()->id;

        Contract::destroy($ride);

        $response = array(
            'message' => 'Contract Deleted'
        );

        return response($response,200);
    }
}
