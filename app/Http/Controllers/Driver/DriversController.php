<?php

namespace App\Http\Controllers\Driver;

use App\Driver;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class DriversController extends Controller
{
    function rand_color() {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Driver::with('user_detail','today_vehicle_assignments')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function users()
    {
        $data = User::where('user_type_id',4)->doesntHave('driver')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'user' => 'required',
            'license_number' => 'required',
            'id_number' => 'required|min:8|max:8',
            'residence' => 'required',
        ));

        $driver = new Driver();
        $driver->user_id = User::where('public_id',$request->user)->first()->id;
        $driver->public_id = Str::random(8);
        $driver->license_number = $request->license_number;
        $driver->id_number = $request->id_number;
        $driver->residence = $request->residence;
        $driver->color = $this->rand_color();
        $driver->save();

        $response = array(
            'data' => 'Driver Created'
        );

        return response($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Driver::where('public_id', $id)->with('user_detail')->first();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'license_number' => 'required',
            'id_number' => 'required|min:8|max:8',
            'residence' => 'required',
        ));

        $driver = Driver::where('public_id',$id)->first();
        $driver->license_number = $request->license_number;
        $driver->id_number = $request->id_number;
        $driver->residence = $request->residence;
        $driver->save();

        $response = array(
            'data' => 'Driver Created'
        );

        return response($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
