<?php

namespace App\Http\Controllers\Position;

use App\Car;
use App\Driver;
use App\VehicleAssingment;
use App\VehicleStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PositionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $position = VehicleStatus::whereDate('created_at', date('Y-m-d'))->where('status',0)->get();

        $response = array(
            'data' => $position
        );

        return response($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $driver = Driver::where('public_id',$id)->first();
        $assignments = VehicleAssingment::where('driver_id',$driver->id)->whereDate('created_at',date('Y-m-d'))->first();

        $position = VehicleStatus::where('vehicle_assignment_id', $assignments->id)->where('status',0)->first();

        $response = array(
            'data' => $position
        );

        return response($response,200);
    }

    public function vehicle($id)
    {
        $car = Car::where('public_id',$id)->first();
        $assignments = VehicleAssingment::where('vehicle_id',$car->id)->whereDate('created_at',date('Y-m-d'))->first();

        $position = VehicleStatus::where('vehicle_assignment_id', $assignments->id)->where('status',0)->first();

        $response = array(
            'data' => $position
        );

        return response($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
