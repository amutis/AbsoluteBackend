<?php

namespace App\Http\Controllers\Rides;

use App\AccountLine;
use App\Company;
use App\Driver;
use App\Jobs\GetETA;
use App\Jobs\MobileNotification;
use App\Jobs\TripCanceling;
use App\Jobs\TripChanges;
use App\Jobs\TripMadeSms;
use App\Ride;
use App\RideType;
use App\User;
use App\VehicleType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class RidesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function user($id)
    {
        $user = User::where('public_id',$id)->first()->id;

        $data = Ride::where('user_id',$user)->with()->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'ride_type' => 'required',
        ));

        $ride_type = RideType::where('public_id',$request->ride_type)->first()->id;

        if ($ride_type == 1){
            $this->validate($request, array(
                'ride_date' => 'required',
                'ride_time' => 'required',
                'passenger' => 'required',
                'vehicle_type' => 'required',
                'ride_type' => 'required',
                'company' => 'required',
                'origin' => 'required',
                'destination' => 'required',
            ));
        }else{
            $this->validate($request, array(
                'ride_date' => 'required',
                'ride_time' => 'required',
                'passenger' => 'required',
                'vehicle_type' => 'required',
                'ride_type' => 'required',
                'origin' => 'required',
                'destination' => 'required',
            ));
        }

        $user = User::where('public_id',$request->passenger)->first();

        $departure = $request->origin;
        $destination = $request->destination;

        $time =  date('H:i',strtotime($request->ride_time)).":00";
        $trip_date = substr($request->ride_date, 0, strpos($request->ride_date, "T"));

        $address = str_replace(" ", "+", $departure); // replace all the white space with "+" sign to match with google search pattern

        $departure_co = Cache::remember($address, 300, function () use ($address) {
            $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
            $response = file_get_contents($url);
            $json = json_decode($response,TRUE); //generate array object from the response from the web
            $departure_co = ($json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng']);

            return $departure_co;
        });

        $address = str_replace(" ", "+", $destination); // replace all the white space with "+" sign to match with google search pattern

        $destination_co = Cache::remember($address, 300, function () use ($address) {
            $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
            $response = file_get_contents($url);
            $json = json_decode($response,TRUE); //generate array object from the response from the web
            $destination_co = ($json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng']);

            return $destination_co;
        });

        $ride = new Ride();
        $ride->public_id = Str::random(8);
        $ride->session_id = Auth::user()->id;
        if ($request->AccountLine != null){
            $ride->account_line = AccountLine::where('public_id',$request->account_line)->first()->id;
        }
        $ride->vehicle_type_id = VehicleType::where('public_id',$request->vehicle_type)->first()->id;
        $ride->ride_type_id = $ride_type;
        $ride->departure_co = $departure_co;
        $ride->departure_name = $departure;
        $ride->destination_co = $destination_co;
        $ride->destination_name = $destination;
        $ride->date_time = $trip_date." ".$time;
        $ride->client_name = $user->first_name.' '.$user->last_name;
        if ($request->company != null){
            $ride->company_id = Company::where('public_id',$request->company)->first()->id;
        }
        $ride->user_id = $user->id;
        $ride->save();

        if ($user->fcm !=null){
            $user_id = $request->passenger;
            $message = ['title'=>'Ride Booked',
                'message'=>'Ride booked for '.$request->ride_date.' at '.$time.'. From '.substr($departure, 0, strpos($departure, ",")).' to '.substr($destination, 0, strpos($destination, ","))
            ];

            $notification = (new MobileNotification($user_id,$message));

            dispatch($notification);
        }


        $eta = (new GetETA($ride));
        $sms = (new TripMadeSms($ride));
        dispatch($sms,$eta);

        $response = array(
            'message' => 'Trip Created'
        );

        return response($response,200);


    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Ride::where('public_id',$id)->with('ride_type','passenger','vehicle_type','company','account_line')->first();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'ride_type' => 'required',
        ));

        $ride_type = RideType::where('public_id',$request->ride_type)->first()->id;

        if ($ride_type == 1){
            $this->validate($request, array(
                'ride_date' => 'required',
                'ride_time' => 'required',
                'passenger' => 'required',
                'vehicle_type' => 'required',
                'ride_type' => 'required',
                'company' => 'required',
                'origin' => 'required',
                'destination' => 'required',
            ));
        }else{
            $this->validate($request, array(
                'ride_date' => 'required',
                'ride_time' => 'required',
                'passenger' => 'required',
                'vehicle_type' => 'required',
                'ride_type' => 'required',
                'origin' => 'required',
                'destination' => 'required',
            ));
        }

        $user = User::where('public_id',$request->passenger)->first();

        $departure = $request->origin;
        $destination = $request->destination;

        $time =  date('H:i',strtotime($request->ride_time)).":00";
        $trip_date = substr($request->ride_date, 0, strpos($request->ride_date, "T"));

        $address = str_replace(" ", "+", $departure); // replace all the white space with "+" sign to match with google search pattern

        $departure_co = Cache::remember($address, 300, function () use ($address) {
            $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
            $response = file_get_contents($url);
            $json = json_decode($response,TRUE); //generate array object from the response from the web
            $departure_co = ($json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng']);

            return $departure_co;
        });

        $address = str_replace(" ", "+", $destination); // replace all the white space with "+" sign to match with google search pattern

        $destination_co = Cache::remember($address, 300, function () use ($address) {
            $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
            $response = file_get_contents($url);
            $json = json_decode($response,TRUE); //generate array object from the response from the web
            $destination_co = ($json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng']);

            return $destination_co;
        });

        $ride = Ride::where('public_id',$id)->first();
        $ride->session_id = Auth::user()->id;
        if ($request->AccountLine != null){
            $ride->account_line = AccountLine::where('public_id',$request->account_line)->first()->id;
        }
        $ride->vehicle_type_id = VehicleType::where('public_id',$request->vehicle_type)->first()->id;
        $ride->ride_type_id = $ride_type;
        $ride->departure_co = $departure_co;
        $ride->departure_name = $departure;
        $ride->destination_co = $destination_co;
        $ride->destination_name = $destination;
        $ride->date_time = $trip_date." ".$time;
        $ride->client_name = $user->first_name.' '.$user->last_name;
        if ($request->company != null){
            $ride->company_id = Company::where('public_id',$request->company)->first()->id;
        }
        $ride->user_id = $user->id;
        $ride->save();

        if ($user->fcm !=null){
            $user_id = $request->passenger;
            $message = ['title'=>'Ride Booked',
                'message'=>'Ride booked for '.$request->ride_date.' at '.$time.'. From '.substr($departure, 0, strpos($departure, ",")).' to '.substr($destination, 0, strpos($destination, ","))
            ];

            $notification = (new MobileNotification($user_id,$message));

            dispatch($notification);
        }

        $sms = (new TripChanges($ride))->delay(Carbon::now()->addMinutes(1));
        dispatch($sms);

        $response = array(
            'message' => 'Trip Updated'
        );

        return response($response,200);

    }

    public function upload(Request $request, $id)
    {

        $file_name = time(). '.' . $request->file('file')->getClientOriginalExtension();

        $request->file('file')->move(
            base_path() . '/public/files/', $file_name
        );

        $ride = Ride::where('public_id',$id)->first();
        $ride->file = $file_name;
        $ride->save();
//        Input::file('file')->move(__DIR__.'/note_files/',Input::file('file')->getClientOriginalName());
//        Storage::put('file.txt', 'Contents');
//        $path = $request->file->store('images');

        $response = array(
            'message' => 'File Updated'
        );

        return response($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ride = Ride::where('public_id',$id)->first()->id;

        Ride::destroy($ride);

        $trip = Ride::onlyTrashed()->where('id',$ride)->first();

        if(date('Y-m-d',strtotime($trip->date_time)) == date('Y-m-d')){
            $date = 'Today';
        }else{
            $date = date('Y-m-d',strtotime($trip->date_time));
        }
        if (User::find($trip->user_id)->fcm !=null){
            $user_id = $trip->user_id;
            $message = ['title'=>'Ride Booked',
                'message'=>'Your ride for '.$date.' at '.date('H:i',strtotime($trip->date_time)).' has been cancelled'
            ];

            $notification = (new MobileNotification($user_id,$message));
            dispatch($notification);
        }

        $sms = (new TripCanceling($trip))->delay(Carbon::now()->addMinutes(1));
        dispatch($sms);

        $response = array(
            'message' => 'Trip deleted'
        );

        return response($response,200);
    }

    public function driver($driver, $ride)
    {
        $driver_id = Driver::where('public_id',$driver)->first()->id;
        $ride_date = Ride::where('public_id',$ride)->first()->date_time;
        $data = Ride::where('driver_id',$driver_id)->whereDate('date_time',date('Y-m-d', strtotime($ride_date)))->orderby('date_time','asc')->with('ride_type','passenger')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    public function unassigned()
    {
        $data = Ride::where('driver_id',null)->orderby('date_time','asc')->with('ride_type','passenger','company')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }


    public function completed()
    {
        $data = Ride::where('status',2)->with('user','type','company')->orderby('updated_at','asc')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    public function assigned()
    {
        $data = Ride::where('vehicle_assignment_id','!=',null)->with('user','type','company')->orderby('date_time','desc')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    public function in_session()
    {
        $data = Ride::where('status',1)->with('type','company')->orderby('updated_at','desc')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    public function note(Request $request, $id)
    {
        $this->validate($request, array(
            'note' => 'required',
        ));

        $ride = Ride::where('public_id',$id)->first();
        $ride->note = $request->note;
        $ride->save();

        $response = array(
            'message' => 'Note Added'
        );

        return response($response,200);
    }

    public function block(Request $request, $id)
    {
        $this->validate($request, array(
            'final_date' => 'required',
            'repeat' => 'required',
        ));


        //Loop
        //If weekly
        $trip = Ride::where('public_id',$id)->first();
        $final_date = substr($request->ride_date, 0, strpos($request->ride_date, "T"));


        if ($request->repeat == 1){
            $trip_date = new Carbon($trip->date_time);
            $future = new Carbon($final_date);
            $difference = ($trip_date->diff($future)->days < 1)
                ?
                : $trip_date->diffInDays($future);

            $weeks = $difference/7;
            $weeks = floor($weeks);

            for ($week = 1; $week <= $weeks; $week++){
                $ride = new Ride();
                $ride->session_id = Auth::user()->id;
                $ride->public_id = Str::random(8);
                $ride->account_line = $trip->account_line_id;
                $ride->vehicle_type_id = $trip->vehicle_type_id;
                $ride->ride_type_id = $trip->ride_type_id;
                $ride->departure_co = $trip->departure_co;
                $ride->departure_name = $trip->departure_name;
                $ride->destination_co = $trip->destination_co;
                $ride->destination_name = $trip->destination_name;
                $ride->date_time = $trip_date->addDays($week  * 7);
                $ride->client_name = $trip->client_name;
                $ride->company_id = $trip->company_id;
                $ride->user_id = $trip->user_id;
                $ride->save();
            }
        }

        if ($request->repeat == 2){
            $trip_date = new Carbon($trip->date_time);
            $future = new Carbon($final_date);
            $difference = ($trip_date->diff($future)->days < 1)
                ?
                : $trip_date->diffInDays($future);

            $days = $difference;

            for ($day = 1; $day <= $days; $day++){
                $ride = new Ride();
                $ride->session_id = Auth::user()->id;
                $ride->public_id = Str::random(8);
                $ride->account_line = $trip->account_line_id;
                $ride->vehicle_type_id = $trip->vehicle_type_id;
                $ride->ride_type_id = $trip->ride_type_id;
                $ride->departure_co = $trip->departure_co;
                $ride->departure_name = $trip->departure_name;
                $ride->destination_co = $trip->destination_co;
                $ride->destination_name = $trip->destination_name;
                $ride->date_time = $trip_date->addDays($day);
                $ride->client_name = $trip->client_name;
                $ride->company_id = $trip->company_id;
                $ride->user_id = $trip->user_id;
                $ride->save();
            }
        }

        $response = array(
            'message' => "Duplication success"
        );

        return response($response,200);
    }
}
