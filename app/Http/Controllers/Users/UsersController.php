<?php

namespace App\Http\Controllers\Users;

use App\Company;
use App\Mail\Welcome;
use App\User;
use App\UserType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::with('company','user_type')->orderBy('first_name', 'asc')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    public function company_count($id)
    {
        $data = User::where('company_id',Company::where('public_id',$id)->first()->id)->count();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    public function company($id)
    {
        $data = User::where('company_id',Company::where('public_id',$id)->first()->id)->with('user_type')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        * Checking user type
        */
        $this->validate($request, array(
            'user_type' => 'required',
        ));

        $user_type = UserType::where('public_id',$request->user_type)->first()->id;
        /*
         * Validation for Normal Client
         */
        if ($user_type == 1 || $user_type == 4){
            $this->validate($request, array(
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'email' => 'required|email|max:255|unique:users',
                'phone_number' => 'required|max:12|min:12|unique:users,phone_number',
                'user_type' => 'required',
            ));
        }elseif ($user_type == 2 || $user_type == 3){
            if (Auth::user()->company_id == true){
                $this->validate($request, array(
                    'first_name' => 'required|max:50',
                    'last_name' => 'required|max:50',
                    'email' => 'required|email|max:255|unique:users',
                    'phone_number' => 'required|max:12|min:12|unique:users,phone_number',
                    'user_type' => 'required'
                ));
            }else {
                $this->validate($request, array(
                    'first_name' => 'required|max:50',
                    'last_name' => 'required|max:50',
                    'email' => 'required|email|max:255|unique:users',
                    'phone_number' => 'required|max:12|min:12|unique:users,phone_number',
                    'user_type' => 'required',
                    'company' => 'required',
                ));
            }

        }
        //Default password
//        $password = "123456";
        //
        $password = Str::random('8');

        $user = new User();
        $user->public_id = Str::random(8);
        $user->user_type_id = UserType::where('public_id',$request->user_type)->first()->id;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        if ($request->company != null){
            $user->company_id = Company::where('public_id',$request->company)->first()->id;
        }elseif (Auth::user()->company_id == true){
            $user->company_id = Auth::user()->company_id;
        }
        $user->password = bcrypt($password);
        $user->save();

        Mail::to($user->email) ->queue(new Welcome($user,$password));


        $response = array(
            'message' => 'User created'
        );

        return response($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::withTrashed()->where('public_id',$id)->withCount('rides')->with('company','rides','user_type')->first();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
     * Checking user type
     */
        $this->validate($request, array(
            'user_type' => 'required',
        ));

        $user_type = UserType::where('public_id',$request->user_type)->first()->id;
        /*
         * Validation for Normal Client
         */
        if ($user_type == 1 || $user_type == 4){
            $this->validate($request, array(
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'email' => 'required|email|max:255',
                'phone_number' => 'required|max:12|min:12',
                'user_type' => 'required',
            ));
        }elseif ($user_type == 2 || $user_type == 3){
            $this->validate($request, array(
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'email' => 'required|email|max:255',
                'phone_number' => 'required|max:12|min:12',
                'user_type' => 'required',
                'company' => 'required',
            ));
        }
        //Default password
//        $password = "123456";
        //

        $user = User::where('public_id',$id)->first();
        $user->user_type_id = UserType::where('public_id',$request->user_type)->first()->id;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        if ($request->company != null){
            $user->company_id = Company::where('public_id',$request->company)->first()->id;
        }else {
            $user->company_id = null;
        }
        $user->save();

//        Mail::to($user->email) ->queue(new Welcome($user,$password));


        $response = array(
            'message' => 'User updated'
        );

        return response($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ride = User::where('public_id',$id)->first()->id;

        User::destroy($ride);

        $response = array(
            'message' => 'User deleted'
        );

        return response($response,200);
    }

    public function deleted()
    {
        $data = User::onlyTrashed()->with('company','user_type')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }
}
