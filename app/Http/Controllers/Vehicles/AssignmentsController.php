<?php

namespace App\Http\Controllers\Vehicles;

use App\Car;
use App\Driver;
use App\Jobs\DriverAssigned;
use App\Jobs\MobileNotification;
use App\Ride;
use App\User;
use App\VehicleAssingment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class AssignmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = VehicleAssingment::all();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'vehicle' => 'required',
            'driver' => 'required',
            'phone_number' => 'required',
        ));

        $owner = new VehicleAssingment();
        $owner->public_id = Str::random(8);
        $owner->vehicle_id = Car::where('public_id',$request->vehicle)->first()->id;
        $owner->driver_id = Driver::where('public_id',$request->driver)->first()->id;
        $owner->phone_number = $request->phone_number;
        $owner->save();

        $response = array(
            'data' => 'messages'
        );

        return response($response,200);
    }

    public function ride(Request $request)
    {

        $this->validate($request, array(
            'ride_id' => 'required',
            'driver_id' => 'required',
        ));

        $driver = Driver::where('public_id', $request->driver_id)->first()->id;
        //Get Assignment
        $assignment = VehicleAssingment::where('driver_id',$driver)->whereDate('created_at',date('Y-m-d'))->first();

        //Get trip
        $trip = Ride::where('public_id', $request->ride_id)->first();

        //Checking if there is an assignment for that day
        if ($assignment != null){
            $trip->vehicle_assignment_id = $assignment->id;
        }
        $trip->driver_id = $driver;
        $trip->save();


        if(date('Y-m-d',strtotime($trip->date_time)) == date('Y-m-d')){
            $date = 'Today';
        }else{
            $date = date('Y-m-d',strtotime($trip->date_time));
        }
        $driver = Driver::find($driver)->user_detail;

        if (User::find($trip->user_id)->fcm !=null){
            $user_id = $trip->user_id;
            $message = ['title'=>'Ride Booked',
                'message'=>$driver->first_name.' '.$driver->last_name.'will be your driver for your ride scheduled for '.$date.' at '.date('H:i',strtotime($trip->date_time))
            ];

            $notification = (new MobileNotification($user_id,$message));


            dispatch( $notification);
        }

//        TODO Alert
//        $sms = (new DriverAssigned($trip))->delay(Carbon::now()->addMinutes(1));
//        $alert_driver = (new AlertDriverOfTripAssignment($trip))->delay(Carbon::now()->addMinutes(1));
//        dispatch($sms,$alert_driver);


        $response = array(
            'message' => 'Assignment successfull'
        );

        return response($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function vehicle($id)
    {
        $data = VehicleAssingment::where('vehicle_id',Car::where('public_id', $id)->first()->id)->with('car','user' )->orderBy('created_at', 'desc')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    public function unassigned_drivers()
    {
        $data = Driver::doesntHave('today_vehicle_assignments')->with('user_detail')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    public function unassigned_vehicles()
    {
        $data = Car::doesntHave('today_assignments')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
