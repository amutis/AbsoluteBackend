<?php

namespace App\Http\Controllers\Vehicles;

use App\Car;
use App\Owner;
use App\VehicleType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class VehiclesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Car::with('owner','vehicle_type')->get();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'vehicle_type' => 'required',
            'phone_number' => 'required',
            'vehicle_owner' => 'required',
            'vehicle_plate' => 'required|max:8|min:8|unique:cars,number_plate',
        ));

        $car = new Car();
        $car->public_id = Str::random(8);
        $car->vehicle_type_id = VehicleType::where('public_id',$request->vehicle_type)->first()->id;
        $car->number_plate = $request->vehicle_plate;
        $car->owner_id = Owner::where('public_id',$request->vehicle_owner)->first()->id;
        $car->phone_number = $request->phone_number;
        $car->save();

        $response = array(
            'message' => 'Vehicle added'
        );

        return response($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Car::where('public_id',$id)->with('owner')->first();

        $response = array(
            'data' => $data
        );

        return response($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'vehicle_type' => 'required',
            'phone_number' => 'required',
            'vehicle_owner' => 'required',
            'vehicle_plate' => 'required|max:8|min:8',
        ));

        $car = Car::where('public_id',$id)->first();
        $car->vehicle_type_id = VehicleType::where('public_id',$request->vehicle_type)->first()->id;
        $car->number_plate = $request->vehicle_plate;
        $car->owner_id = Owner::where('public_id',$request->vehicle_owner)->first()->id;
        $car->phone_number = $request->phone_number;
        $car->save();

        $response = array(
            'message' => 'Vehicle added'
        );

        return response($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
