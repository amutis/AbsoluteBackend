<?php

namespace App\Jobs;

use App\Ride;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DriverAssigned implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Ride $ride)
    {


        $message = $ride->passenger->first_name." A driver, ".$ride->driver->user_detail->first_name.", has been assigned to your trip. The vehicle plate is ".$ride->assignment->car->number_plate;
        $phone = $ride->passenger->phone_number;


        $sendsms = str_replace(' ', '+',$message);

        $username = "absolutesafaris";
        $password = "absolute_safaris.";


        $parameters = "username=$username&password=$password&destination=$phone&message=$sendsms";
        $url="http://www.mobisky.biz/api/sendsms2.php?$parameters";

        return $response = file_get_contents($url);

    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}
