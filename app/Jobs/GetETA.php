<?php

namespace App\Jobs;

use App\Ride;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GetETA implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Ride $ride)
    {
            /*
             * Getting ETA
             */
            $departure_name = $ride->departure_name;
            $departure_name = str_replace(" ", "+", $departure_name);
            $destination_name = $ride->destination_name;
            $destination_name = str_replace(" ", "+", $destination_name);
            $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$departure_name."&destinations=".$destination_name."&departure_time=".strtotime($ride->date_time)."&mode=driving&sensor=true";
            $response = file_get_contents($url);
            $json = json_decode($response,TRUE); //generate array object from the response from the web
            $eta = ($json['rows'][0]['elements'][0]['duration']['value']);
            $ride->eta = $eta;
            $ride->eta_end_date_time =  date('Y-m-d h:i:s',strtotime($ride->date_time)+$eta);
            $ride->save();

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}
