<?php

namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class MobileNotification implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id,$message)
    {
        $user = User::find($user_id);


        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($message['title']);
        $notificationBuilder->setBody($message['message'])
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($message);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

//        $token = "flr2cFm3KPM:APA91bF4IzcjWBqDU4oZjTzUtxrbdlc9De9ybgSXp6xmsSjTsX3RF075V53WhkMd-qldMcv0ZsX1WkPdw4cQKYe4yK5CU9F8NseaqVHyj-YYJK9KvEmJYlZ6TG45QjbTKdarPPTae2hc";
        $token = $user->fcm;

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

       return $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}
