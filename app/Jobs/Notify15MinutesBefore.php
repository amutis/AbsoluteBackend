<?php

namespace App\Jobs;

use App\Ride;
use Carbon\Carbon;
use Ftg\Sms\Facades\Sms;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class Notify15MinutesBefore implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Ride $trip)
    {
        //Send message to driver

        //Getting pick up and drop place from db
        $pick_up_place = $trip->departure_name;
        $destination = $trip->destination_name;

        //getting the name place
        $pick_up_place_array = explode(",",$pick_up_place);
        $pick_up_place = $pick_up_place_array[0];

        $destination_array = explode(",",$destination);
        $destination = $destination_array[0];

        //Send message to Passenger
//        $passenger_mobile = "254732730473";
        $passenger_mobile = $trip->passenger->phone_number;
        $message = "Your ride from ".$pick_up_place.' to '.$destination.' is 15 minutes away. '.$trip->driver->user_detail->first_name.' '.$trip->driver->user_detail->last_name.' of phone number '.$trip->driver->user_detail->phone_number.' will be your driver';

        $sms = (new SendSms15Before($passenger_mobile,$message))->delay(date('Y-m-d H:i:s',strtotime($trip->date_time."+ 15 minutes")));
//        $sms = (new SendSms15Before($passenger_mobile,$message))->delay(Carbon::now()->addMinutes(10));
        dispatch($sms);

        //Send message to driver
        //        TODO change to driver number by commenting and uncommenting the necessary lines
        $driver_mobile = "254716666309";
//        $driver_mobile = $trip->driver->user_detail->phone_number;
        $message = "Your ride from ".$pick_up_place.' to '.$destination.' is 15 minutes away. '.$trip->passenger->first_name.' '.$trip->passenger->last_name.' of phone number '.$trip->passenger->phone_number.' will be waiting.';
//        Sms::send_sms($driver_mobile,$message);

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}
