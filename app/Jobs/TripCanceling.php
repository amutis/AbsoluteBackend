<?php

namespace App\Jobs;

use App\Ride;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TripCanceling implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Ride $ride)
    {


        $message = $ride->passenger->first_name.", Your Trip from ".substr($ride->departure_name, 0, strpos($ride->departure_name, ","))." to ". substr($ride->destination_name, 0, strpos($ride->destination_name, ","))." has been cancelled";
        $phone = $ride->passenger->phone_number;


        $sendsms = str_replace(' ', '+',$message);

        $username = "absolutesafaris";
        $password = "absolute_safaris.";


        $parameters = "username=$username&password=$password&destination=$phone&message=$sendsms";
        $url="http://www.mobisky.biz/api/sendsms2.php?$parameters";

        return $response = file_get_contents($url);

    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}
