<?php

namespace App\Jobs;

use App\Ride;
use App\VehicleAssingment;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateVehicleAssignment implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(VehicleAssingment $vehicleAssingment)
    {
        $driver = $vehicleAssingment->driver_id;
        $today_trips = Ride::whereDate('date_time',date('Y-m-d'))->where('driver_id',$driver)->update(['vehicle_assignment_id' => $vehicleAssingment->id]);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}
