<?php

namespace App\Mail;

use App\Ride;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PrivateInvoice extends Mailable
{
    use Queueable, SerializesModels;
    public $ride;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Ride $ride)
    {
        $this->ride = $ride;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@absoluteadventuresafaris.com')
            ->subject('Absolute Invoice')
            ->view('mails.private_invoice')
            ->with([
                'cost' => $this->ride->amount,
                'client' => $this->ride->passenger->first_name,
                'start_time' => date('H:i',strtotime($this->ride->departure_time)),
                'stop_time' => date('H:i',strtotime($this->ride->arrival_time)),
                'pick_up_point' => substr($this->ride->departure_name, 0, strpos($this->ride->departure_name, ",")),
                'drop_off' => substr($this->ride->destination_name, 0, strpos($this->ride->destination_name, ",")),
            ]);
    }
}
