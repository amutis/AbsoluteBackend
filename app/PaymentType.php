<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentType extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function rides()
    {
        return $this->hasMany('App\Ride','payment_type_id','id');
    }
}
