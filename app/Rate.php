<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rate extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function company()
    {
        return $this->hasOne('App\Company','id','company_id');
    }
}
