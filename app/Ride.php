<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ride extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function payment_type()
    {
        return $this->hasOne('App\PaymentType','id','payment_type_id');
    }

    /**
     *
     **/
    public function account_line()
    {
        return $this->hasOne('App\AccountLine','id','account_line_id');
    }

    /**
     *
     **/
    public function ride_type()
    {
        return $this->hasOne('App\RideType','id','ride_type_id');
    }

    /**
     *
     **/
    public function ride_costs()
    {
        return $this->hasMany('App\RideCost','ride_id','id');
    }

    /**
     *
     **/
    public function passenger()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    /**
     *
     **/
    public function type()
    {
        return $this->hasOne('App\RideType','id','ride_type_id');
    }

    /**
     *
     **/
    public function company()
    {
        return $this->hasOne('App\Company','id','company_id');
    }

    /**
     *
     **/
    public function assignment()
    {
        return $this->hasOne('App\VehicleAssingment','id','vehicle_assignment_id');
    }

    /**
     *
     **/
    public function driver()
    {
        return $this->hasOne('App\Driver','id','driver_id');
    }

    /**
     *
     **/
    public function user()
    {
        return $this->driver()->with('user_detail');
    }

    /**
     *
     **/
    public function session()
    {
        return $this->hasOne('App\User','id','session_id');
    }


    ///API

    /**
     *
     **/
    public function api_passenger()
    {
        return $this->hasOne('App\User','id','user_id')->select('first_name','last_name','email','phone_number');
    }

    /**
     *
     **/
    public function api_session()
    {
        return $this->hasOne('App\User','id','session_id')->select('first_name','last_name','email','phone_number');
    }

    /**
     *
     **/
    public function api_type()
    {
        return $this->hasOne('App\RideType','id','ride_type_id')->select('name','description');
    }

    /**
     *
     **/
    public function api_company()
    {
        return $this->hasOne('App\Company','id','company_id')->select('name');
    }

    /**
     *
     **/
    public function tariff()
    {
        return $this->hasOne('App\Tarrif','id','tariff_id');
    }

    /**
     *
     **/
    public function vehicle_type()
    {
        return $this->hasOne('App\VehicleType','id','vehicle_type_id');
    }

}
