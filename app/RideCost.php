<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RideCost extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function cost_type()
    {
        return $this->hasOne('App\Cost','id','cost_type_id');
    }

    /**
     *
     **/
    public function ride()
    {
        return $this->hasOne('App\Ride','id','ride_id');
    }
}
