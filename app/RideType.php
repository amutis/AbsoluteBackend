<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RideType extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function rides()
    {
        return $this->hasMany('App\Ride','ride_type_id','id');
    }
}
