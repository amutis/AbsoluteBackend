<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','phone_number','company_id', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     *
     **/
    public function company()
    {
        return $this->hasOne('App\Company','id','company_id');
    }

    /**
     *
     **/
    public function rides()
    {
        return $this->hasMany('App\Ride','user_id','id');
    }

    /**
     *
     **/
    public function user_type()
    {
        return $this->hasOne('App\UserType','id','user_type_id');
    }

    /**
     *
     **/
    public function driver()
    {
        return $this->hasOne('App\Driver','user_id','id');
    }

    //API

    /**
     *
     **/
    public function api_driver()
    {
        return $this->hasOne('App\Driver','user_id','id');
    }
}
