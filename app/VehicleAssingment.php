<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleAssingment extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function traffics()
    {
        return $this->hasMany('App\BaseTraffic','vehicle_assignment_id','id');
    }

    /**
     *
     **/
    public function car()
    {
        return $this->hasOne('App\Car','id','vehicle_id');
    }

    /**
     *
     **/
    public function car_today()
    {
        return $this->hasOne('App\Car','id','vehicle_id')->whereDate('created_at',date('Y-m-d'));
    }

    /**
     *
     **/
    public function rides()
    {
        return $this->hasMany('App\Ride','vehicle_assignment','id');
    }

    /**
     *
     **/
    public function assignment_costs()
    {
        return $this->hasMany('App\AssignmentCost','vehicle_assignment_id','id');
    }

    /**
     *
     **/
    public function driver()
    {
        return $this->hasOne('App\Driver','id','driver_id');
    }

    public function user()
    {
        return $this->driver()->with('user_detail');
    }

    //API
    /**
     *
     **/
    public function api_car()
    {
        return $this->hasOne('App\Car','id','vehicle_id')->select('id','number_plate');
    }

    /**
     *
     **/
    public function api_driver()
    {
        return $this->hasOne('App\Driver','id','driver_id');
    }

    /**
     *
     **/
    public function api_position()
    {
        return $this->hasOne('App\VehicleStatus','vehicle_assignment_id','id')->select('lat','long')->where('status',0);
    }
}
