<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleStatus extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function vehicle_assignment()
    {
        return $this->hasOne('App\VehicleAssingment','id','vehicle_assignment_id');
    }
}
