<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleType extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function cars()
    {
        return $this->hasMany('App\Car','vehicle_type_id','id');
    }


    /**
     *
     **/
    public function rides()
    {
        return $this->hasOne('App\Ride','vehicle_type_id','id');
    }
}
