<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('public_id')->unique();
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('email')->unique();
            $table->string('telephone_1')->unique();
            $table->string('telephone_2')->nullable()->unique();
            $table->string('country_id')->index();
            $table->string('town');
            $table->string('street');
            $table->string('building');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
