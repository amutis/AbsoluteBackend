<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseTrafficsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_traffics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('public_id')->unique();
            $table->integer('base_id')->index();
            $table->integer('vehicle_assignment_id')->index();
            $table->time('arrival_time');
            $table->time('departure_time');
            $table->integer('status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_traffics');
    }
}
