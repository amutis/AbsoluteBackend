<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rides', function (Blueprint $table) {
            $table->increments('id');
            $table->string('public_id')->unique();
            $table->integer('session_id')->index();
            $table->integer('account_line')->index()->nullable();
            $table->integer('vehicle_type_id')->nullable()->index();
            $table->integer('company_id')->nullable()->index();
            $table->integer('tarrif_id')->nullable()->index();
            $table->string('departure_co')->index();
            $table->string('departure_name')->nullable();
            $table->string('destination_co')->index();
            $table->string('destination_name')->nullable();
            $table->dateTime('date_time');
            $table->dateTime('eta_end_date_time')->nullable();
            $table->integer('eta')->nullable();
            $table->string('file')->nullable();
            $table->string('client_name')->nullable();
            $table->integer('user_id')->nullable()->index();
            $table->integer('vehicle_assignment_id')->nullable()->index();
            $table->integer('driver_id')->nullable()->index();
            $table->double('amount')->nullable();
            $table->integer('ride_type_id')->index();
            $table->time('departure_time')->nullable();
            $table->time('arrival_time')->nullable();
            $table->double('distance')->nullable();
            $table->double('duration')->nullable();
            $table->integer('tariff_id')->nullable();
            $table->text('note')->nullable();
            $table->text('driver_note')->nullable();
            $table->integer('payment_type_id')->nullable()->index();
            $table->integer('status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rides');
    }
}
