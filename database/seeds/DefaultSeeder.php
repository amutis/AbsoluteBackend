<?php

use Illuminate\Database\Seeder;

class DefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('system_defaults')->truncate();

         $data = [
             ['public_id' => \Illuminate\Support\Str::random(8), 'name' => 'Cost Per Km', 'amount' => '40','description'=>'amount charged in kenyan shillings per kilometer for a private ride'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'name' => 'Cost Per min', 'amount' => '4','description'=>'amount charged in kenyan shillings per minute for a private ride'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'name' => 'Minimum Charge', 'amount' => '300','description'=>'The least amount that can ever be charged'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'name' => 'waiting time', 'amount' => '3','description'=>'amount per minute'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'name' => 'half day', 'amount' => '3000','description'=>'amount when vehicle is hired for 5 hours'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'name' => 'full day', 'amount' => '7000','description'=>'amount when vehicle is hired for 10 hours'],
         ];

         DB::table('system_defaults')->insert($data);
    }
}
