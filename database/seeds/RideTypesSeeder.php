<?php

use Illuminate\Database\Seeder;

class RideTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('ride_types')->truncate();

         $data = [
             ['public_id' => \Illuminate\Support\Str::random(8), 'slug' => 'corprate_ride' , 'name' => 'Corporate Ride', 'description' => 'Corporate rides'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'slug' => 'private_ride' , 'name' => 'Private Ride', 'description' => 'Private rides '],
             ['public_id' => \Illuminate\Support\Str::random(8), 'slug' => 'half_day' , 'name' => 'Half Day', 'description' => 'Half day ride'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'slug' => 'full_day' , 'name' => 'Full Day', 'description' => 'Full day ride'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'slug' => 'long_distance' , 'name' => 'Long Distance', 'description' => 'Rides over 100 km'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'slug' => 'fixed' , 'name' => 'Fixed ', 'description' => 'School pickups and dropping'],
         ];

         DB::table('ride_types')->insert($data);
    }
}
