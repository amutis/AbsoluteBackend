<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->truncate();

         $data = [
             ['public_id' => \Illuminate\Support\Str::random(8), 'user_type_id' => '5', 'first_name' => 'Trial','last_name' => 'Admin','phone_number'=>'254700000000','email' => 'admin@absolute.com','password' => '$2y$10$JpKqkzxr9Nn2.wI2CtmkjOAFjlsRvnOEItnBQmBeZh50VAzLltHRS'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'user_type_id' => '4', 'first_name' => 'Trial','last_name' => 'Driver','phone_number'=>'254711111111','email' => 'driver@absolute.com','password' => '$2y$10$JpKqkzxr9Nn2.wI2CtmkjOAFjlsRvnOEItnBQmBeZh50VAzLltHRS'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'user_type_id' => '3', 'first_name' => 'Company','last_name' => 'User','phone_number'=>'254733333333','email' => 'companyuser@absolute.com','password' => '$2y$10$JpKqkzxr9Nn2.wI2CtmkjOAFjlsRvnOEItnBQmBeZh50VAzLltHRS'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'user_type_id' => '2', 'first_name' => 'Company','last_name' => 'User','phone_number'=>'254744444444','email' => 'companyclient@absolute.com','password' => '$2y$10$JpKqkzxr9Nn2.wI2CtmkjOAFjlsRvnOEItnBQmBeZh50VAzLltHRS'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'user_type_id' => '1', 'first_name' => 'Normal','last_name' => 'User','phone_number'=>'254755555555','email' => 'user@absolute.com','password' => '$2y$10$JpKqkzxr9Nn2.wI2CtmkjOAFjlsRvnOEItnBQmBeZh50VAzLltHRS'],
         ];

         DB::table('users')->insert($data);
    }
}
