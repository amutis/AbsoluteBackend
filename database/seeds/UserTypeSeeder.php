<?php

use Illuminate\Database\Seeder;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('user_types')->truncate();

         $data = [
             ['public_id' => \Illuminate\Support\Str::random(8), 'name' => 'Client', 'description' => 'A normal client'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'name' => 'Company Client', 'description' => 'A client under a company'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'name' => 'Company Admin', 'description' => 'A user under a company'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'name' => 'Driver', 'description' => 'A user under Absolute that handles vehicles'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'name' => 'Admin', 'description' => 'A user under a company'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'name' => 'Company Sub-Admin', 'description' => 'A company user who can create rides and all but not see reports and add users'],
         ];

         DB::table('user_types')->insert($data);
    }
}
