<?php

use Illuminate\Database\Seeder;

class VehicleTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('vehicle_types')->truncate();

         $data = [
             ['public_id' => \Illuminate\Support\Str::random(8), 'name' => 'Saloons / Sedans', 'price'=>'40','description' => 'A class described as "large family" in Europe and "mid-size" in the USA, these cars have room for five adults and a large trunk (boot). Engines are more powerful than small family/compact cars and six-cylinder engines are more common than in smaller cars. Car sizes vary from region to region; in Europe, large family cars are rarely over 4,700 mm (15.4 ft) long, while in North America, Middle East and Australasia they may be well over 4,800 mm (15.7 ft).'],
             ['public_id' => \Illuminate\Support\Str::random(8), 'name' => 'Vans', 'price'=>'100', 'description' => 'data'],
         ];

         DB::table('vehicle_types')->insert($data);
    }
}
