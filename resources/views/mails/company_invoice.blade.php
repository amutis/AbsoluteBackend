<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta content="telephone=no" name="format-detection" />
    <title>Company Invoice</title>


    <style type="text/css" media="screen">
        /* Linked Styles */
        body { padding:0 !important; margin:0 !important; display:block !important; background:#ffffff; -webkit-text-size-adjust:none }
        a { color:#00b8e4; text-decoration:underline }
        h3 a { color:#1f1f1f; text-decoration:none }
        .text2 a { color:#ea4261; text-decoration:none }


        /* Campaign Monitor wraps the text in editor in paragraphs. In order to preserve design spacing we remove the padding/margin */
        p { padding:0 !important; margin:0 !important }
    </style>
</head>
<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; background:#ffffff; -webkit-text-size-adjust:none">

<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
    <tr>
        <td align="center" valign="top">
            <table width="800" border="0" cellspacing="0" cellpadding="0">

                <!-- Content -->
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="90"></td>
                                <td>
                                    <div style="font-size:0pt; line-height:0pt; height:30px"><img src="images/empty.gif" width="1" height="30" style="height:30px" alt="" /></div>

                                    <div>
                                        <div class="h1" style="color:#1f1f1f; font-family:Tahoma; font-size:30px; line-height:24px; text-align:center; font-weight:bold">
                                            <div>Ride Invoice</div>
                                        </div>
                                        <div style="font-size:0pt; line-height:0pt; height:10px"><img src="images/empty.gif" width="1" height="10" style="height:10px" alt="" /></div>


                                        <div class="text-center" style="color:#868686; font-family:Tahoma; font-size:14px; line-height:18px;">
                                            <div>
                                                <h1>KES : {{$cost}}</h1>
                                                <br />
                                                <p>It was a pleasure riding with, {{$client}}. To view all your invoices, login to the web application or request for an account creation.
                                                <br>
                                                <br>
                                                <hr>
                                                <br>
                                                </p>
                                                {{$start_time}} : {{$pick_up_point}}
                                                <br>
                                                {{$stop_time}} : {{$drop_off}}
                                        </div>
                                        <div style="font-size:0pt; line-height:0pt; height:35px"><img src="images/empty.gif" width="1" height="35" style="height:35px" alt="" /></div>

                                            </div>
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- END Content -->
                <!-- Footer -->
                <tr>
                    <td>
                        <div class="img" style="font-size:0pt; line-height:0pt; text-align:left"><img src="images/footer_top.jpg" alt="" border="0" width="800" height="10" /></div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeefec">
                            <tr>
                                <td>
                                    <div style="font-size:0pt; line-height:0pt; height:12px"><img src="images/empty.gif" width="1" height="12" style="height:12px" alt="" /></div>

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#" target="_blank"><img src="images/facebook.jpg" alt="" border="0" width="43" height="43" /></a></td>
                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="7"></td>
                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#" target="_blank"><img src="images/twitter.jpg" alt="" border="0" width="43" height="43" /></a></td>
                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="7"></td>
                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#" target="_blank"><img src="images/behance.jpg" alt="" border="0" width="43" height="43" /></a></td>
                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="7"></td>
                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#" target="_blank"><img src="images/dribble.jpg" alt="" border="0" width="43" height="43" /></a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="font-size:0pt; line-height:0pt; height:30px"><img src="images/empty.gif" width="1" height="30" style="height:30px" alt="" /></div>

                                    <div class="footer" style="color:#a9aaa9; font-family:Arial; font-size:11px; line-height:20px; text-align:center">
                                        <div>
                                            Address, Republic of Design www.Link.com email@sitename.com<br />
                                            Copyright &copy; <span>{{date('Y')}}</span> <span>Absolute Safaris</span>.
                                        </div>
                                        <a class="link5-u" style="color:#a9aaa9; text-decoration:underline" target="_blank" href="#">Unsubscribe</a>
                                    </div>
                                    <div style="font-size:0pt; line-height:0pt; height:25px"><img src="images/empty.gif" width="1" height="25" style="height:25px" alt="" /></div>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- END Footer -->
            </table>
        </td>
    </tr>
</table>

</body>
</html>
