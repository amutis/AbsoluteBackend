<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta content="telephone=no" name="format-detection" />
    <title>Welcome</title>


    <style type="text/css" media="screen">
        /* Linked Styles */
        body { padding:0 !important; margin:0 !important; display:block !important; background:#ffffff; -webkit-text-size-adjust:none }
        a { color:#00b8e4; text-decoration:underline }
        h3 a { color:#1f1f1f; text-decoration:none }
        .text2 a { color:#ea4261; text-decoration:none }


        /* Campaign Monitor wraps the text in editor in paragraphs. In order to preserve design spacing we remove the padding/margin */
        p { padding:0 !important; margin:0 !important }
    </style>
</head>
<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; background:#ffffff; -webkit-text-size-adjust:none">

<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
    <tr>
        <td align="center" valign="top">
            <table width="800" border="0" cellspacing="0" cellpadding="0">

                <!-- Content -->
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="90"></td>
                                <td>
                                    <div style="font-size:0pt; line-height:0pt; height:30px"><img src="images/empty.gif" width="1" height="30" style="height:30px" alt="" /></div>

                                    <p>Hi , {{$first_name}}</p>
                                    <div>
                                        <div class="h2" style="color:#1f1f1f; font-family:Tahoma; font-size:20px; line-height:24px; text-align:center; font-weight:bold">
                                            <div>Welcome to Absolute Safaris</div>
                                        </div>
                                        <div style="font-size:0pt; line-height:0pt; height:10px"><img src="images/empty.gif" width="1" height="10" style="height:10px" alt="" /></div>


                                        <div class="text-center" style="color:#868686; font-family:Tahoma; font-size:14px; line-height:18px; text-align:center">
                                            <div>Your account has been successfully created. The email in use is the receiving email of this mail. <br /> <br /> Your login details are <br /> <strong>Email : </strong>{{$email}}<br /> <strong>Password : </strong>{{$password}}<br /> <br />
                                                <a href="{{url('/')}}/login"><button class="btn btn-primary">Login</button></a></div>
                                        </div>
                                        <div style="font-size:0pt; line-height:0pt; height:35px"><img src="images/empty.gif" width="1" height="35" style="height:35px" alt="" /></div>

                                    </div>

                                    <div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td valign="top" width="130">
                                                    <div class="img-center" style="font-size:0pt; line-height:0pt; text-align:center"><a href="#" target="_blank"><img src="images/img1.jpg" alt="" border="0" width="109" height="109" /></a></div>
                                                    <div style="font-size:0pt; line-height:0pt; height:30px"><img src="images/empty.gif" width="1" height="30" style="height:30px" alt="" /></div>


                                                    <div class="h3" style="color:#1f1f1f; font-family:Tahoma; font-size:16px; line-height:20px; text-align:center; font-weight:normal">
                                                        <div>Book from your Mobile</div>
                                                    </div>
                                                    <div style="font-size:0pt; line-height:0pt; height:15px"><img src="images/empty.gif" width="1" height="15" style="height:15px" alt="" /></div>


                                                    <div class="text2-center" style="color:#bebebe; font-family:Tahoma; font-size:12px; line-height:18px; text-align:center">
                                                        <div>
                                                            Lorem ipsum dolamet, consectetuer  elit, sed diam nonummy nibh euismo
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="33"></td>
                                                <td valign="top" width="130">
                                                    <div class="img-center" style="font-size:0pt; line-height:0pt; text-align:center"><a href="#" target="_blank"><img src="images/img2.jpg" alt="" border="0" width="109" height="109" /></a></div>
                                                    <div style="font-size:0pt; line-height:0pt; height:30px"><img src="images/empty.gif" width="1" height="30" style="height:30px" alt="" /></div>


                                                    <div class="h3" style="color:#1f1f1f; font-family:Tahoma; font-size:16px; line-height:20px; text-align:center; font-weight:normal">
                                                        <div>On the Go Notifications</div>
                                                    </div>
                                                    <div style="font-size:0pt; line-height:0pt; height:15px"><img src="images/empty.gif" width="1" height="15" style="height:15px" alt="" /></div>


                                                    <div class="text2-center" style="color:#bebebe; font-family:Tahoma; font-size:12px; line-height:18px; text-align:center">
                                                        <div>
                                                            Lorem ipsum dolamet, consectetuer  elit, sed diam nonummy nibh euismo
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="33"></td>
                                                <td valign="top" width="130">
                                                    <div class="img-center" style="font-size:0pt; line-height:0pt; text-align:center"><a href="#" target="_blank"><img src="images/img3.jpg" alt="" border="0" width="109" height="109" /></a></div>
                                                    <div style="font-size:0pt; line-height:0pt; height:30px"><img src="images/empty.gif" width="1" height="30" style="height:30px" alt="" /></div>


                                                    <div class="h3" style="color:#1f1f1f; font-family:Tahoma; font-size:16px; line-height:20px; text-align:center; font-weight:normal">
                                                        <div>Manage your own Company</div>
                                                    </div>
                                                    <div style="font-size:0pt; line-height:0pt; height:15px"><img src="images/empty.gif" width="1" height="15" style="height:15px" alt="" /></div>


                                                    <div class="text2-center" style="color:#bebebe; font-family:Tahoma; font-size:12px; line-height:18px; text-align:center">
                                                        <div>
                                                            Lorem ipsum dolamet, consectetuer  elit, sed diam nonummy nibh euismo
                                                        </div>
                                                    </div>
                                                </td>

                                                </td>
                                            </tr>
                                        </table>
                                        <div style="font-size:0pt; line-height:0pt; height:10px"><img src="images/empty.gif" width="1" height="10" style="height:10px" alt="" /></div>

                                    </div>

                                    <div>
                                        <div style="font-size:0pt; line-height:0pt; height:20px"><img src="images/empty.gif" width="1" height="20" style="height:20px" alt="" /></div>

                                        <div class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#" target="_blank"><img src="images/separator.jpg" alt="" border="0" width="620" height="13" /></a></div>
                                        <div style="font-size:0pt; line-height:0pt; height:20px"><img src="images/empty.gif" width="1" height="20" style="height:20px" alt="" /></div>

                                    </div>
                                </td>
                                <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="90"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- END Content -->
                <!-- Footer -->
                <tr>
                    <td>
                        <div class="img" style="font-size:0pt; line-height:0pt; text-align:left"><img src="images/footer_top.jpg" alt="" border="0" width="800" height="10" /></div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeefec">
                            <tr>
                                <td>
                                    <div style="font-size:0pt; line-height:0pt; height:12px"><img src="images/empty.gif" width="1" height="12" style="height:12px" alt="" /></div>

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#" target="_blank"><img src="images/facebook.jpg" alt="" border="0" width="43" height="43" /></a></td>
                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="7"></td>
                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#" target="_blank"><img src="images/twitter.jpg" alt="" border="0" width="43" height="43" /></a></td>
                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="7"></td>
                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#" target="_blank"><img src="images/behance.jpg" alt="" border="0" width="43" height="43" /></a></td>
                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="7"></td>
                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#" target="_blank"><img src="images/dribble.jpg" alt="" border="0" width="43" height="43" /></a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="font-size:0pt; line-height:0pt; height:30px"><img src="images/empty.gif" width="1" height="30" style="height:30px" alt="" /></div>

                                    <div class="footer" style="color:#a9aaa9; font-family:Arial; font-size:11px; line-height:20px; text-align:center">
                                        <div>
                                            Address, Republic of Design www.Link.com email@sitename.com<br />
                                            Copyright &copy; <span>{{date('Y')}}</span> <span>Absolute Safaris</span>.
                                        </div>
                                        <a class="link5-u" style="color:#a9aaa9; text-decoration:underline" target="_blank" href="#">Unsubscribe</a>
                                    </div>
                                    <div style="font-size:0pt; line-height:0pt; height:25px"><img src="images/empty.gif" width="1" height="25" style="height:25px" alt="" /></div>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- END Footer -->
            </table>
        </td>
    </tr>
</table>

</body>
</html>
