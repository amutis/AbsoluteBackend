<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::post('password/reset', 'Api\UserController@reset_password')->middleware('auth:api');

Route::post('request/ride', 'Api\Trip@request_ride')->middleware('auth:api');
Route::get('ride/distance/{id}', 'Api\Trip@getRideDistance')->middleware('auth:api');
Route::get('ride/all', 'Api\Trip@getRides')->middleware('auth:api');
Route::get('commissions', 'Api\Commissions@getCommission')->middleware('auth:api');
Route::post('new_user', 'Api\UserController@register_user');
Route::get('assignment_details', 'Api\DriverController@assignment_details')->middleware('auth:api');
Route::get('upcoming_trips', 'Api\Trip@upcoming_trips')->middleware('auth:api');
Route::get('future_trips', 'Api\Trip@future_trips')->middleware('auth:api');
Route::post('fcm', 'Api\MobileNotifications@update_fcm')->middleware('auth:api');
Route::get('not', 'Api\MobileNotifications@send_user_notification');
Route::get('vehicle_types', 'Api\VehiclesController@get_vehicle_types');


Route::get('check_notifications','Api\MobileNotifications@check_notification')->middleware('auth:api');

Route::get('ride/find_driver_location/{trip_id}', 'Api\PositionsController@position_of_coming_car')->middleware('auth:api');
Route::get('ride/find_trip_location/{trip_id}', 'Api\PositionsController@position_of_trip')->middleware('auth:api');
Route::get('available_vehicles', 'Api\VehiclesController@available_vehicles')->middleware('auth:api');
Route::post('position', 'Api\PositionsController@update_position')->middleware('auth:api');
Route::post('start_trip', 'Api\Trip@trip_start')->middleware('auth:api');
Route::post('end_trip', 'Api\Trip@end_trip')->middleware('auth:api');

Route::get('user/coming_car', 'Api\PositionsController@position_of_coming_car')->middleware('auth:api');
Route::get('user/trip_position', 'Api\PositionsController@position_of_trip')->middleware('auth:api');

Route::post('sos', 'Api\SosController@doSos')->middleware('auth:api');

Route::post('store/place', 'Api\StoredPlacesController@store_place')->middleware('auth:api');
Route::get('stored/places', 'Api\StoredPlacesController@stored_places')->middleware('auth:api');


