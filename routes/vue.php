<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//  Urls
Route::prefix('transport_categories')->middleware('auth:api')->group(function () {
    Route::post('post', 'Users\TransportCategoriesController@store');
    Route::get('/', 'Users\TransportCategoriesController@index');
    Route::get('single/{id}', 'Users\TransportCategoriesController@show');
    Route::get('delete/{id}', 'Users\TransportCategoriesController@destroy');
    Route::post('edit/{id}', 'Users\TransportCategoriesController@update');

    Route::get('company/count/{id}', 'Users\TransportCategoriesController@company');
});

// Users Urls
Route::prefix('users')->middleware('auth:api')->group(function () {
    Route::post('post', 'Users\UsersController@store');
    Route::get('all', 'Users\UsersController@index');
    Route::get('single/{id}', 'Users\UsersController@show');
    Route::get('delete/{id}', 'Users\UsersController@destroy');
    Route::post('edit/{id}', 'Users\UsersController@update');

    Route::get('company/count/{id}', 'Users\UsersController@company_count');
    Route::get('company/{id}', 'Users\UsersController@company');
    Route::get('deleted', 'Users\UsersController@deleted');
    Route::get('reinstate/{id}', 'Users\UsersController@reinstate');
});

//  User Types Urls
Route::prefix('user_types')->middleware('auth:api')->group(function () {
    Route::post('post', 'Users\UserTypesController@store');
    Route::get('/', 'Users\UserTypesController@index');
    Route::get('single/{id}', 'Users\UserTypesController@show');
    Route::get('delete/{id}', 'Users\UserTypesController@destroy');
    Route::post('edit/{id}', 'Users\UserTypesController@update');
});

/**
 * Companies Folder
 */

// Company Urls
Route::prefix('companies')->middleware('auth:api')->group(function () {
    Route::post('post', 'Companies\CompaniesController@store');
    Route::get('/', 'Companies\CompaniesController@index');
    Route::get('single/{id}', 'Companies\CompaniesController@show');
    Route::get('delete/{id}', 'Companies\CompaniesController@destroy');
    Route::post('edit/{id}', 'Companies\CompaniesController@update');
});

// Account Lines Urls
Route::prefix('account_lines')->middleware('auth:api')->group(function () {
    Route::post('post', 'Companies\AccountLinesController@store');
    Route::get('/', 'Companies\AccountLinesController@index');
    Route::get('single/{id}', 'Companies\AccountLinesController@show');
    Route::get('delete/{id}', 'Companies\AccountLinesController@destroy');
    Route::post('edit/{id}', 'Companies\AccountLinesController@update');

    Route::get('company/{id}', 'Companies\AccountLinesController@company');

});

// Account Lines Urls
Route::prefix('zones')->middleware('auth:api')->group(function () {
    Route::post('post', 'Companies\NoGoZonesController@store');
    Route::get('/', 'Companies\NoGoZonesController@index');
    Route::get('company/{id}', 'Companies\NoGoZonesController@company');
    Route::get('single/{id}', 'Companies\NoGoZonesController@show');
    Route::get('delete/{id}', 'Companies\NoGoZonesController@destroy');
    Route::post('edit/{id}', 'Companies\NoGoZonesController@update');
});

// Contract Urls
Route::prefix('contracts')->middleware('auth:api')->group(function () {
    Route::post('post/{id}', 'Companies\ContractsController@store');
    Route::get('/', 'Companies\ContractsController@index');
    Route::get('company/{id}', 'Companies\ContractsController@company');
    Route::get('single/{id}', 'Companies\ContractsController@show');
    Route::get('delete/{id}', 'Companies\ContractsController@destroy');
    Route::post('edit/{id}', 'Companies\ContractsController@update');
});

/**
 * Vehicles Folder
 */

// Account Lines Urls
Route::prefix('owners')->middleware('auth:api')->group(function () {
    Route::post('post', 'Vehicles\OwnersController@store');
    Route::get('/', 'Vehicles\OwnersController@index');
    Route::get('single/{id}', 'Vehicles\OwnersController@show');
    Route::get('delete/{id}', 'Vehicles\OwnersController@destroy');
    Route::post('edit/{id}', 'Vehicles\OwnersController@update');
});

// Vehicle Status Urls
Route::prefix('vehicle_status')->middleware('auth:api')->group(function () {
    Route::post('post', 'Vehicles\VehicleStatusController@store');
    Route::get('/', 'Vehicles\VehicleStatusController@index');
    Route::get('single/{id}', 'Vehicles\VehicleStatusController@show');
    Route::get('delete/{id}', 'Vehicles\VehicleStatusController@destroy');
    Route::post('edit/{id}', 'Vehicles\VehicleStatusController@update');
});

// Vehicle Types Urls
Route::prefix('vehicle_types')->middleware('auth:api')->group(function () {
    Route::post('post', 'Vehicles\VehicleTypesController@store');
    Route::get('/', 'Vehicles\VehicleTypesController@index');
    Route::get('single/{id}', 'Vehicles\VehicleTypesController@show');
    Route::get('delete/{id}', 'Vehicles\VehicleTypesController@destroy');
    Route::post('edit/{id}', 'Vehicles\VehicleTypesController@update');
});

// Vehicles Urls
Route::prefix('vehicles')->middleware('auth:api')->group(function () {
    Route::post('post', 'Vehicles\VehiclesController@store');
    Route::get('/', 'Vehicles\VehiclesController@index');
    Route::get('single/{id}', 'Vehicles\VehiclesController@show');
    Route::get('delete/{id}', 'Vehicles\VehiclesController@destroy');
    Route::post('edit/{id}', 'Vehicles\VehiclesController@update');
});

// Vehicle Owners Urls
Route::prefix('vehicle_owners')->group(function () {
    Route::post('post', 'Vehicles\OwnersController@store');
    Route::get('/', 'Vehicles\OwnersController@index');
    Route::get('single/{id}', 'Vehicles\OwnersController@show');
    Route::get('delete/{id}', 'Vehicles\OwnersController@destroy');
    Route::post('edit/{id}', 'Vehicles\OwnersController@update');
});

// Assignment Urls
Route::prefix('assignments')->middleware('auth:api')->group(function () {
    Route::post('post', 'Vehicles\AssignmentsController@store');
    Route::post('ride', 'Vehicles\AssignmentsController@ride');
    Route::get('/', 'Vehicles\AssignmentsController@index');
    Route::get('vehicle/{id}', 'Vehicles\AssignmentsController@vehicle');
    Route::get('single/{id}', 'Vehicles\AssignmentsController@show');
    Route::get('delete/{id}', 'Vehicles\AssignmentsController@destroy');
    Route::post('edit/{id}', 'Vehicles\AssignmentsController@update');
    Route::get('unassigned/drivers', 'Vehicles\AssignmentsController@unassigned_drivers');
    Route::get('unassigned/vehicles', 'Vehicles\AssignmentsController@unassigned_vehicles');
});

/**
 * Resource Folder
 */

// Countries Urls
Route::prefix('countries')->middleware('auth:api')->group(function () {
    Route::post('post', 'Resource\CountriesController@store');
    Route::get('/', 'Resource\CountriesController@index');
    Route::get('single/{id}', 'Resource\CountriesController@show');
    Route::get('delete/{id}', 'Resource\CountriesController@destroy');
    Route::post('edit/{id}', 'Resource\CountriesController@update');
});

/**
 * Rides Folder
 */

// Rides Urls
Route::prefix('rides')->middleware('auth:api')->group(function () {
    Route::post('post', 'Rides\RidesController@store');
    Route::get('/', 'Rides\RidesController@index');
    Route::get('assigned', 'Rides\RidesController@assigned');
    Route::get('completed', 'Rides\RidesController@completed');
    Route::get('unassigned', 'Rides\RidesController@unassigned');
    Route::get('in_session', 'Rides\RidesController@in_session');
    Route::get('single/{id}', 'Rides\RidesController@show');
    Route::get('delete/{id}', 'Rides\RidesController@destroy');
    Route::get('driver/{driver_id}/{ride_id}', 'Rides\RidesController@driver');
    Route::post('edit/{id}', 'Rides\RidesController@update');
    Route::post('note/{id}', 'Rides\RidesController@note');
    Route::post('upload/{id}', 'Rides\RidesController@upload');
    Route::post('block/{id}', 'Rides\RidesController@block');
    Route::get('user/{id}', 'Rides\RidesController@user');
});

// Rides Urls
Route::prefix('ride_types')->middleware('auth:api')->group(function () {
    Route::post('post', 'Rides\RideTypesController@store');
    Route::get('/', 'Rides\RideTypesController@index');
    Route::get('single/{id}', 'Rides\RideTypesController@show');
    Route::get('delete/{id}', 'Rides\RideTypesController@destroy');
    Route::post('edit/{id}', 'Rides\RideTypesController@update');
});

/**
 * Driver Folder
 */

// Driver Urls
Route::prefix('drivers')->middleware('auth:api')->group(function () {
    Route::post('post', 'Driver\DriversController@store');
    Route::get('/', 'Driver\DriversController@index');
    Route::get('single/{id}', 'Driver\DriversController@show');
    Route::get('delete/{id}', 'Driver\DriversController@destroy');
    Route::get('users', 'Driver\DriversController@users');
    Route::post('edit/{id}', 'Driver\DriversController@update');
});

/**
 * Position Folder
 */

// Position Urls
Route::prefix('positions')->middleware('auth:api')->group(function () {
    Route::post('post', 'Position\PositionsController@store');
    Route::get('/', 'Position\PositionsController@index');
    Route::get('vehicle/{id}', 'Position\PositionsController@vehicle');
    Route::get('single/{id}', 'Position\PositionsController@show');
    Route::get('delete/{id}', 'Position\PositionsController@destroy');
    Route::get('users', 'Position\PositionsController@users');
    Route::post('edit/{id}', 'Position\PositionsController@update');
});